/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oh.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import oh.data.TimeSlot.DayOfWeek;

/**
 *
 * @author Michael
 */
public class TAOfficeHours {
    DayOfWeek day;
    StringProperty time;
    
    public TAOfficeHours (DayOfWeek day, String time) {
        this.day = day;
        this.time = new SimpleStringProperty(time);
    }
    
    public String getStartTime() {
        return time.getValue();
    }
    
    public DayOfWeek getDayOfWeek() {
        return day;
    }
    
}
