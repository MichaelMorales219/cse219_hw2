package oh.data;

import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import djf.modules.AppGUIModule;
import static java.time.DayOfWeek.*;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_GRADUATE_TA_RADIO_BUTTON;
import static oh.OfficeHoursPropertyType.OH_OFFICE_HOURS_TABLE_VIEW;
import static oh.OfficeHoursPropertyType.OH_TAS_TABLE_VIEW;
import static oh.OfficeHoursPropertyType.OH_UNDERGRADUATE_TA_RADIO_BUTTON;
import oh.data.TimeSlot.DayOfWeek;

/**
 * This is the data component for TAManagerApp. It has all the data needed
 * to be set by the user via the User Interface and file I/O can set and get
 * all the data from this object
 * 
 * @author Richard McKenna
 */
public class OfficeHoursData implements AppDataComponent {

    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    OfficeHoursApp app;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<TeachingAssistantPrototype> teachingAssistants;
    ArrayList<TeachingAssistantPrototype> taList = new ArrayList<>();
    ObservableList<TimeSlot> officeHours;
    

    // THESE ARE THE TIME BOUNDS FOR THE OFFICE HOURS GRID. NOTE
    // THAT THESE VALUES CAN BE DIFFERENT FOR DIFFERENT FILES, BUT
    // THAT OUR APPLICATION USES THE DEFAULT TIME VALUES AND PROVIDES
    // NO MEANS FOR CHANGING THESE VALUES
    int startHour;
    int endHour;
    
    // DEFAULT VALUES FOR START AND END HOURS IN MILITARY HOURS
    public static final int MIN_START_HOUR = 9;
    public static final int MAX_END_HOUR = 20;

    /**
     * This constructor will setup the required data structures for
     * use, but will have to wait on the office hours grid, since
     * it receives the StringProperty objects from the Workspace.
     * 
     * @param initApp The application this data manager belongs to. 
     */
    public OfficeHoursData(OfficeHoursApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        AppGUIModule gui = app.getGUIModule();

        // CONSTRUCT THE LIST OF TAs FOR THE TABLE
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        teachingAssistants = taTableView.getItems();
       
        for (TeachingAssistantPrototype ta: teachingAssistants) {
            taList.add(ta);
        }
        
        // THESE ARE THE DEFAULT OFFICE HOURS
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        
        resetOfficeHours();
    }
    
    private void resetOfficeHours() {
        //THIS WILL STORE OUR OFFICE HOURS
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView)gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        officeHours = officeHoursTableView.getItems(); 
        officeHours.clear();
        for (int i = startHour; i <= endHour; i++) {
            TimeSlot timeSlot = new TimeSlot(   this.getTimeString(i, true),
                                                this.getTimeString(i, false));
            officeHours.add(timeSlot);
            
            TimeSlot halfTimeSlot = new TimeSlot(   this.getTimeString(i, false),
                                                    this.getTimeString(i+1, true));
            officeHours.add(halfTimeSlot);
        }
    }
    
    private String getTimeString(int militaryHour, boolean onHour) {
        String minutesText = "00";
        if (!onHour) {
            minutesText = "30";
        }

        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutesText;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }
    
    public void initHours(String startHourText, String endHourText) {
        int initStartHour = Integer.parseInt(startHourText);
        int initEndHour = Integer.parseInt(endHourText);
        if (initStartHour <= initEndHour) {
            // THESE ARE VALID HOURS SO KEEP THEM
            // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
            startHour = initStartHour;
            endHour = initEndHour;
        }
        resetOfficeHours();
    }
        
    /**
     * Called each time new work is created or loaded, it resets all data
     * and data structures such that they can be used for new values.
     */
    @Override
    public void reset() {
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        teachingAssistants.clear();
        
        for (TimeSlot timeSlot : officeHours) {
            timeSlot.reset();
        }
    }
    
    // ACCESSOR METHODS

    public int getStartHour() {
        return startHour;
    }

    public int getEndHour() {
        return endHour;
    }
    
    public boolean isTASelected() {
        AppGUIModule gui = app.getGUIModule();
        TableView tasTable = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        return tasTable.getSelectionModel().getSelectedItem() != null;
    }
    
    public void addTA(TeachingAssistantPrototype ta) {
        if (!this.teachingAssistants.contains(ta)) {
            this.teachingAssistants.add(ta);
        }
        if (!this.taList.contains(ta)) {
            this.taList.add(ta);
        } 
    }
    
    public void removeTA(TeachingAssistantPrototype ta) {
        // REMOVE THE TA FROM THE LIST OF TAs
        this.teachingAssistants.remove(ta);
        this.taList.remove(ta);
        
        // AND REMOVE THE TA FROM ALL THEIR OFFICE HOURS
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        ObservableList<TimeSlot> tslist = officeHoursTableView.getItems();
        
         for (TimeSlot tslot: tslist) {
            if (tslot.tas.get(DayOfWeek.MONDAY).contains(ta)) {
                tslot.tas.get(DayOfWeek.MONDAY).remove(ta);
                tslot.updateDayText(DayOfWeek.MONDAY);
            } 
            if(tslot.tas.get(DayOfWeek.TUESDAY).contains(ta)) {
                tslot.tas.get(DayOfWeek.TUESDAY).remove(ta);
                tslot.updateDayText(DayOfWeek.TUESDAY);
            }  
            if(tslot.tas.get(DayOfWeek.WEDNESDAY).contains(ta)) {
                tslot.tas.get(DayOfWeek.WEDNESDAY).remove(ta);
                tslot.updateDayText(DayOfWeek.WEDNESDAY);
            }  
            if(tslot.tas.get(DayOfWeek.THURSDAY).contains(ta)) {
                tslot.tas.get(DayOfWeek.THURSDAY).remove(ta);
                tslot.updateDayText(DayOfWeek.THURSDAY);
            } 
            if(tslot.tas.get(DayOfWeek.FRIDAY).contains(ta)) {
                tslot.tas.get(DayOfWeek.FRIDAY).remove(ta);
                tslot.updateDayText(DayOfWeek.FRIDAY);
            }
        }
         
    }

    public boolean isLegalNewTA(String name, String email, String type) {
        if ((name.trim().length() > 0)
                && (email.trim().length() > 0)) {
            // MAKE SURE NO TA ALREADY HAS THE SAME NAME
            TeachingAssistantPrototype testTA = new TeachingAssistantPrototype(name, email, type);
            if (this.taList.contains(testTA))
                return false;
            if (this.isLegalNewEmail(email)) {
                return true && !radioButtonSelected().equals("All");
            }
        }
        return false;
    }
    
    public boolean isLegalNewName(String name, String email, String type) {
        TeachingAssistantPrototype testTA = new TeachingAssistantPrototype(name, email, type);
            if (this.taList.contains(testTA))
                return false;
        return true;
    }
    
    public boolean isLegalNewEmail(String email) {
        Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
                "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
        if (matcher.find()) {
            for (TeachingAssistantPrototype ta : this.taList) {
                if (ta.getEmail().equals(email.trim()))
                    return false;
            }
            return true;
        }
        else return false;
    }
    
    public ObservableList sortTAList(ObservableList<TeachingAssistantPrototype> taList) {
        String current;
        String next;
        TeachingAssistantPrototype temp;
        for (int i = 0; i < taList.size() - 1; i++) {
            int tempI = i;
            for (int j = i + 1; j > 0; j--) {
                current = taList.get(tempI).getName();
                next = taList.get(j).getName();
                if (current.compareToIgnoreCase(next) > 0) {
                    //switch
                    temp = taList.get(i);
                    taList.set(i, taList.get(j));
                    taList.set(j, temp); 
                    tempI--;
                } else {
                    break;
                }
            }
        }
        return taList;
    }
    
    public void refreshTATable() {
    
        ArrayList<TeachingAssistantPrototype> temp = new ArrayList<>();
        
        for (TeachingAssistantPrototype ta: taList) {
            temp.add(ta);
        }
        
        if (!temp.isEmpty()) {
            if (!this.radioButtonSelected().equals("All")) {
               for (int i  = 0; i < temp.size(); i++) {
                   String type = this.radioButtonSelected();
                    if (!temp.get(i).getType().equals(this.radioButtonSelected())) {
                        temp.remove(i);
                        i--;
                    }
                }
            } else if(this.radioButtonSelected().equals("All")) {
                for (TeachingAssistantPrototype ta: taList) {
                    if (!temp.contains(ta)) {
                        temp.add(ta);
                    }
                }
            }
            
            teachingAssistants.clear();
            
            for (TeachingAssistantPrototype ta: temp) {
                teachingAssistants.add(ta);
            }
            
            teachingAssistants = this.sortTAList(teachingAssistants);
        }
    }
    
    
    public void refreshOHTable() {
        TableView<TimeSlot> officeHoursTableView = (TableView) app.getGUIModule().getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        ObservableList<TimeSlot> tslist = officeHoursTableView.getItems();
        String type;
        
        if (this.radioButtonSelected().equals("All")) {
             for (TimeSlot tslot: tslist) {
                    tslot.updateDayText(DayOfWeek.MONDAY);                             
                    tslot.updateDayText(DayOfWeek.TUESDAY);               
                    tslot.updateDayText(DayOfWeek.WEDNESDAY);
                    tslot.updateDayText(DayOfWeek.THURSDAY);
                    tslot.updateDayText(DayOfWeek.FRIDAY);
             }
        } else {
            if (this.radioButtonSelected().equals("Graduate")) {
                type = "Graduate";
            } else {
                 type = "Undergraduate";
            }
            StringProperty newDayText = new SimpleStringProperty("");
            for (TimeSlot tslot: tslist) {
                String taText = "";
                int counter = 0;
                for (TeachingAssistantPrototype ta: tslot.tas.get(DayOfWeek.MONDAY)) {
                    if (ta.getType().equals(type)) {
                         taText += ta.getName().substring(0, ta.getName().indexOf(" "));
                        if (counter < ((tslot.tas.get(DayOfWeek.MONDAY)).size()-1)) {
                            taText += "\n";
                        }
                    }
                    tslot.dayText.get(DayOfWeek.MONDAY).setValue(taText);
                    counter++;
                }
                  
                taText = "";
                counter = 0;
                
                for (TeachingAssistantPrototype ta: tslot.tas.get(DayOfWeek.TUESDAY)) {
                    if (ta.getType().equals(type)) {
                         taText += ta.getName().substring(0, ta.getName().indexOf(" "));
                        if (counter < ((tslot.tas.get(DayOfWeek.TUESDAY)).size()-1)) {
                            taText += "\n";
                        }
                    }
                    tslot.dayText.get(DayOfWeek.TUESDAY).setValue(taText);
                    counter++;
                }
                
                taText = "";
                counter = 0;
                
                for (TeachingAssistantPrototype ta: tslot.tas.get(DayOfWeek.WEDNESDAY)) {
                    if (ta.getType().equals(type)) {
                         taText += ta.getName().substring(0, ta.getName().indexOf(" "));
                        if (counter < ((tslot.tas.get(DayOfWeek.WEDNESDAY)).size()-1)) {
                            taText += "\n";
                        }
                    }
                    tslot.dayText.get(DayOfWeek.WEDNESDAY).setValue(taText);
                    counter++;
                }
                
                taText = "";
                counter = 0;
                
                for (TeachingAssistantPrototype ta: tslot.tas.get(DayOfWeek.THURSDAY)) {
                    if (ta.getType().equals(type)) {
                         taText += ta.getName().substring(0, ta.getName().indexOf(" "));
                        if (counter < ((tslot.tas.get(DayOfWeek.THURSDAY)).size()-1)) {
                            taText += "\n";
                        }
                    }
                    tslot.dayText.get(DayOfWeek.THURSDAY).setValue(taText);
                    counter++;
                }
                
                taText = "";
                counter = 0;
                
                for (TeachingAssistantPrototype ta: tslot.tas.get(DayOfWeek.FRIDAY)) {
                    if (ta.getType().equals(type)) {
                         taText += ta.getName().substring(0, ta.getName().indexOf(" "));
                        if (counter < ((tslot.tas.get(DayOfWeek.FRIDAY)).size()-1)) {
                            taText += "\n";
                        }
                    }
                    tslot.dayText.get(DayOfWeek.FRIDAY).setValue(taText);
                    counter++;
                }
            }
        }
    }
    
      
    public void editTA(TeachingAssistantPrototype oldTA, TeachingAssistantPrototype editTA, int row) {
        
        TableView<TimeSlot> tasTableView = (TableView) app.getGUIModule().getGUINode(OH_TAS_TABLE_VIEW);

        ObservableList tas = tasTableView.getItems();
        
        tas.remove(oldTA);
        this.removeTA(oldTA);
        tas.add(editTA);
        this.addTA(editTA);
        
        this.updateOHTable(editTA, oldTA);
    }
    
    public void updateOHTable(TeachingAssistantPrototype currentTA, TeachingAssistantPrototype oldTA) {
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        ObservableList<TimeSlot> tslist = officeHoursTableView.getItems();
        
        for (TimeSlot tslot: tslist) {
            if (tslot.tas.get(DayOfWeek.MONDAY).contains(oldTA)) {
                tslot.tas.get(DayOfWeek.MONDAY).remove(oldTA);
                tslot.tas.get(DayOfWeek.MONDAY).add(currentTA);
                tslot.updateDayText(DayOfWeek.MONDAY);
            } 
            if(tslot.tas.get(DayOfWeek.TUESDAY).contains(oldTA)) {
                tslot.tas.get(DayOfWeek.TUESDAY).remove(oldTA);
                tslot.tas.get(DayOfWeek.TUESDAY).add(currentTA);
                tslot.updateDayText(DayOfWeek.TUESDAY);
            }  
            if(tslot.tas.get(DayOfWeek.WEDNESDAY).contains(oldTA)) {
                tslot.tas.get(DayOfWeek.WEDNESDAY).remove(oldTA);
                tslot.tas.get(DayOfWeek.WEDNESDAY).add(currentTA);
                tslot.updateDayText(DayOfWeek.WEDNESDAY);
            }  
            if(tslot.tas.get(DayOfWeek.THURSDAY).contains(oldTA)) {
                tslot.tas.get(DayOfWeek.THURSDAY).remove(oldTA);
                tslot.tas.get(DayOfWeek.THURSDAY).add(currentTA);
                tslot.updateDayText(DayOfWeek.THURSDAY);
            } 
            if(tslot.tas.get(DayOfWeek.FRIDAY).contains(oldTA)) {
                tslot.tas.get(DayOfWeek.FRIDAY).remove(oldTA);
                tslot.tas.get(DayOfWeek.FRIDAY).add(currentTA);
                tslot.updateDayText(DayOfWeek.FRIDAY);
            }
        }
    }
    
    public ArrayList getOfficeHours(TeachingAssistantPrototype ta) {
        ArrayList<TAOfficeHours> taTslot = new ArrayList<>();
 
        
        for (TimeSlot tslot: officeHours) {
            
            if (tslot.tas.get(DayOfWeek.MONDAY).contains(ta)) {
                taTslot.add(new TAOfficeHours(DayOfWeek.MONDAY, tslot.getStartTime()));
            }  
            if(tslot.tas.get(DayOfWeek.TUESDAY).contains(ta)) {
                taTslot.add(new TAOfficeHours(DayOfWeek.TUESDAY, tslot.getStartTime()));
            } 
            if(tslot.tas.get(DayOfWeek.WEDNESDAY).contains(ta)) {
                taTslot.add(new TAOfficeHours(DayOfWeek.WEDNESDAY, tslot.getStartTime()));
            } 
            if(tslot.tas.get(DayOfWeek.THURSDAY).contains(ta)) {
                taTslot.add(new TAOfficeHours(DayOfWeek.THURSDAY, tslot.getStartTime()));
            } 
            if(tslot.tas.get(DayOfWeek.FRIDAY).contains(ta)) {
                taTslot.add(new TAOfficeHours(DayOfWeek.FRIDAY, tslot.getStartTime()));
            }
        }
        
        return taTslot;
    } 
    
    public void putTAOH(ArrayList<TAOfficeHours> tslots, TeachingAssistantPrototype ta) {
        for (TAOfficeHours taoh: tslots) {
            for (TimeSlot tslot: officeHours) {
                if (tslot.getStartTime().equals(taoh.getStartTime())) {
                    tslot.tas.get(taoh.getDayOfWeek()).add(ta);
                }
            }
        }
    }
    
    public void highlightCells(TeachingAssistantPrototype currentTA) {
        AppGUIModule gui = app.getGUIModule();
        
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        ObservableList<TimeSlot> tslist = officeHoursTableView.getItems();
        officeHoursTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
                
        officeHoursTableView.getSelectionModel().clearSelection();
        
        for (TimeSlot tslot: tslist) {
            if (tslot.tas.get(DayOfWeek.MONDAY).contains(currentTA)) {
                officeHoursTableView.getSelectionModel().select(tslot.getIntStartTime(getStartHour()), officeHoursTableView.getColumns().get(2));
            } 
            if(tslot.tas.get(DayOfWeek.TUESDAY).contains(currentTA)) {
                officeHoursTableView.getSelectionModel().select(tslot.getIntStartTime(getStartHour()), officeHoursTableView.getColumns().get(3));
            } 
            if(tslot.tas.get(DayOfWeek.WEDNESDAY).contains(currentTA)) {
                officeHoursTableView.getSelectionModel().select(tslot.getIntStartTime(getStartHour()), officeHoursTableView.getColumns().get(4));
            } 
            if(tslot.tas.get(DayOfWeek.THURSDAY).contains(currentTA)) {
                officeHoursTableView.getSelectionModel().select(tslot.getIntStartTime(getStartHour()), officeHoursTableView.getColumns().get(5));
            } 
            if(tslot.tas.get(DayOfWeek.FRIDAY).contains(currentTA)) {
                officeHoursTableView.getSelectionModel().select(tslot.getIntStartTime(getStartHour()), officeHoursTableView.getColumns().get(6));
            }
        }
    }
    
    public String radioButtonSelected() {
        RadioButton graduate = (RadioButton) app.getGUIModule().getGUINode(OH_GRADUATE_TA_RADIO_BUTTON);
        RadioButton undergrad = (RadioButton) app.getGUIModule().getGUINode(OH_UNDERGRADUATE_TA_RADIO_BUTTON);
        if (graduate.isSelected()) {
            return "Graduate";
        } else if (undergrad.isSelected()) {
            return "Undergraduate";
        } else  {
            return "All";
        }
        
    }
    
    public boolean isDayOfWeekColumn(int columnNumber) {
        return columnNumber >= 2;
    }
    
    public DayOfWeek getColumnDayOfWeek(int columnNumber) {
        return TimeSlot.DayOfWeek.values()[columnNumber-2];
    }

    public Iterator<TeachingAssistantPrototype> teachingAssistantsIterator() {
        return taList.iterator();
    }
    
    public Iterator<TimeSlot> officeHoursIterator() {
        return officeHours.iterator();
    }

    public TeachingAssistantPrototype getTAWithName(String name) {
        Iterator<TeachingAssistantPrototype> taIterator = teachingAssistants.iterator();
        while (taIterator.hasNext()) {
            TeachingAssistantPrototype ta = taIterator.next();
            if (ta.getName().equals(name))
                return ta;
        }
        return null;
    }

    public TeachingAssistantPrototype getTAWithEmail(String email) {
        Iterator<TeachingAssistantPrototype> taIterator = teachingAssistants.iterator();
        while (taIterator.hasNext()) {
            TeachingAssistantPrototype ta = taIterator.next();
            if (ta.getEmail().equals(email))
                return ta;
        }
        return null;
    }

    public TimeSlot getTimeSlot(String startTime) {
        Iterator<TimeSlot> timeSlotsIterator = officeHours.iterator();
        while (timeSlotsIterator.hasNext()) {
            TimeSlot timeSlot = timeSlotsIterator.next();
            String timeSlotStartTime = timeSlot.getStartTime().replace(":", "_");
            if (timeSlotStartTime.equals(startTime))
                return timeSlot;
        }
        return null;
    }
}