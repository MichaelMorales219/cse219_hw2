/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oh.workspace.dialogs;

import djf.ui.AppNodesBuilder;
import javafx.beans.Observable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import static javafx.scene.paint.Color.*;
import javafx.scene.text.*;
import javafx.stage.Stage;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_EDIT_TA_CANCEL_BUTTON_TEXT;
import static oh.OfficeHoursPropertyType.OH_EDIT_TA_DIALOG_LABEL;
import static oh.OfficeHoursPropertyType.OH_EDIT_TA_EMAIL_LABEL;
import static oh.OfficeHoursPropertyType.OH_EDIT_TA_NAME_LABEL;
import static oh.OfficeHoursPropertyType.OH_EDIT_TA_OK_BUTTON_TEXT;
import static oh.OfficeHoursPropertyType.OH_EDIT_TA_TYPE_LABEL;
import static oh.OfficeHoursPropertyType.OH_GRID_PANE;
import static oh.OfficeHoursPropertyType.OH_UNDERGRADUATE_TA_RADIO_BUTTON_TEXT;
import static oh.OfficeHoursPropertyType.OH_GRADUATE_TA_RADIO_BUTTON_TEXT;
import oh.workspace.controllers.OfficeHoursController;
import static oh.workspace.style.OHStyle.CLASS_OH_PANE;
import properties_manager.PropertiesManager;

/**
 *
 * @author Michael
 */
public class EditTADialog extends Stage{
    
    OfficeHoursApp app;
    
    GridPane gridPane;
    Label editTATitle;
    Label nameLabel;
    Label emailLabel;
    Label typeLabel;
    
    TextField nameField;
    TextField emailField;
    
    RadioButton graduate;
    RadioButton undergraduate;
    
    Button cancelBtn;
    Button okBtn;

    public EditTADialog(OfficeHoursApp initApp) {
        app = initApp;
        OfficeHoursController controller = new OfficeHoursController((OfficeHoursApp) app);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        gridPane = new GridPane();

        String editTATitleText = props.getProperty(OH_EDIT_TA_DIALOG_LABEL);
        editTATitle = new Label(editTATitleText);
        
        String editTANameText = props.getProperty(OH_EDIT_TA_NAME_LABEL);
        nameLabel = new Label(editTANameText);
        
        String editTAEmailText = props.getProperty(OH_EDIT_TA_EMAIL_LABEL);
        emailLabel = new Label(editTAEmailText);
        
        String editTATypeText = props.getProperty(OH_EDIT_TA_TYPE_LABEL);
        typeLabel = new Label(editTATypeText);
        
        nameField = new TextField();
        emailField = new TextField();
        
        ToggleGroup group = new ToggleGroup();
        undergraduate = new RadioButton();
        graduate = new RadioButton();
        
        graduate.setToggleGroup(group);
        undergraduate.setToggleGroup(group);
        
        String graduateBtnText = props.getProperty(OH_GRADUATE_TA_RADIO_BUTTON_TEXT);
        String undergraduateBtnText = props.getProperty(OH_UNDERGRADUATE_TA_RADIO_BUTTON_TEXT);
        
        graduate.setText(graduateBtnText);
        undergraduate.setText(undergraduateBtnText);
        graduate.selectedProperty().setValue(true);
        
        cancelBtn = new Button();
        okBtn = new Button();
        
        String okBtnText = props.getProperty(OH_EDIT_TA_OK_BUTTON_TEXT);
        String cancelBtnText = props.getProperty(OH_EDIT_TA_CANCEL_BUTTON_TEXT);
        
        cancelBtn.setText(cancelBtnText);
        okBtn.setText(okBtnText);
        
        
        //ADD NODES TO PANE
        gridPane.add(editTATitle, 0, 0);
        gridPane.add(nameLabel, 0, 1);
        gridPane.add(emailLabel, 0, 2);
        gridPane.add(typeLabel, 0, 3);
        
        gridPane.add(nameField, 1, 1);
        gridPane.add(emailField, 1, 2);
        
        gridPane.add(graduate, 1, 3);
        gridPane.add(undergraduate, 2, 3);
        
        gridPane.add(okBtn, 1, 4);
        gridPane.add(cancelBtn, 2, 4);
        
        gridPane.setHgap(5);
        gridPane.setVgap(5);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        
        Scene dialogScene = new Scene(gridPane);
        
        //DESIGN
        dialogScene.setFill(Color.web("DCE8E7"));
        gridPane.setBackground(Background.EMPTY);
        editTATitle.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        nameLabel.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        emailLabel.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        typeLabel.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        nameField.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        emailField.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        graduate.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        undergraduate.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        okBtn.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        cancelBtn.setFont(Font.font("Roboto", FontWeight.BOLD, 14));
        
        this.setScene(dialogScene);
        
        
        //CONTROLLER INITIALIZATION
        nameField.textProperty().addListener((Observable e)->{
            RadioButton selected = ((RadioButton)group.getSelectedToggle());
            String gradStatus = selected.textProperty().getValue();
            controller.processTypeEditTA(nameField.getText(), emailField.getText(), gradStatus, okBtn, nameField, emailField);
        });
        emailField.textProperty().addListener(e->{
            RadioButton selected = ((RadioButton)group.getSelectedToggle());
            String gradStatus = selected.textProperty().getValue();
            controller.processTypeEditTA(nameField.getText(), emailField.getText(), gradStatus, okBtn, nameField, emailField);
        });
        
        okBtn.setOnAction(e -> {
            this.hide();
            //CALL METHOD THAT CHANGES TA
             RadioButton selected = ((RadioButton)group.getSelectedToggle());
             String gradStatus = selected.textProperty().getValue();
        
            controller.editTAInformation(nameField.getText(), emailField.getText(), gradStatus);
            
            controller.refreshOHTable();
        });
        cancelBtn.setOnAction(e -> {
            this.hide();
        });
        
    }
}
