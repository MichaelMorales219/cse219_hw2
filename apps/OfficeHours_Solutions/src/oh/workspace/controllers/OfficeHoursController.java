package oh.workspace.controllers;

import djf.modules.AppGUIModule;
import djf.ui.dialogs.AppDialogsFacade;
import djf.ui.dialogs.AppWelcomeDialog;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_ADD_TA_BUTTON;
import static oh.OfficeHoursPropertyType.OH_EMAIL_TEXT_FIELD;
import static oh.OfficeHoursPropertyType.OH_FOOLPROOF_SETTINGS;
import static oh.OfficeHoursPropertyType.OH_GRADUATE_TA_RADIO_BUTTON;
import static oh.OfficeHoursPropertyType.OH_NAME_TEXT_FIELD;
import static oh.OfficeHoursPropertyType.OH_NO_TA_SELECTED_CONTENT;
import static oh.OfficeHoursPropertyType.OH_NO_TA_SELECTED_TITLE;
import static oh.OfficeHoursPropertyType.OH_OFFICE_HOURS_TABLE_VIEW;
import static oh.OfficeHoursPropertyType.OH_TAS_TABLE_VIEW;
import static oh.OfficeHoursPropertyType.OH_UNDERGRADUATE_TA_RADIO_BUTTON;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.data.TimeSlot;
import oh.data.TimeSlot.DayOfWeek;
import oh.transactions.AddTA_Transaction;
import oh.transactions.EditTA_Transaction;
import oh.transactions.ToggleOfficeHours_Transaction;
import oh.workspace.dialogs.EditTADialog;
import oh.workspace.foolproof.OfficeHoursFoolproofDesign;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursController {

    OfficeHoursApp app;

    public OfficeHoursController(OfficeHoursApp initApp) {
        app = initApp;
    }

    public void processAddTA() {
        AppGUIModule gui = app.getGUIModule();
        TextField nameTF = (TextField) gui.getGUINode(OH_NAME_TEXT_FIELD);
        String name = nameTF.getText();
        TextField emailTF = (TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD);
        String email = emailTF.getText();
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        String type = data.radioButtonSelected();
        
        if (data.isLegalNewTA(name, email, type) && !type.equals("All")) {
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name.trim(), email.trim(), type);
            AddTA_Transaction addTATransaction = new AddTA_Transaction(data, ta);
            app.processTransaction(addTATransaction);

            // NOW CLEAR THE TEXT FIELDS
            nameTF.setText("");
            emailTF.setText("");
            nameTF.requestFocus();
        }
    }

    public void processVerifyTA() {
        
    }

    public void refreshTATable() {
         OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
         data.refreshTATable();     
    }
    
    public void refreshOHTable() {
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        data.refreshOHTable();
    }
    
    public void processToggleOfficeHours() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        ObservableList<TablePosition> selectedCells = officeHoursTableView.getSelectionModel().getSelectedCells();
        if (selectedCells.size() > 0) {
            TablePosition cell = selectedCells.get(0);
            int cellColumnNumber = cell.getColumn();
            OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
            if (data.isDayOfWeekColumn(cellColumnNumber)) {
                DayOfWeek dow = data.getColumnDayOfWeek(cellColumnNumber);
                TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
                TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
                if (ta != null) {
                    TimeSlot timeSlot = officeHoursTableView.getSelectionModel().getSelectedItem();
                    ToggleOfficeHours_Transaction transaction = new ToggleOfficeHours_Transaction(data, timeSlot, dow, ta);
                    app.processTransaction(transaction);
                }
                else {
                    Stage window = app.getGUIModule().getWindow();
                    AppDialogsFacade.showMessageDialog(window, OH_NO_TA_SELECTED_TITLE, OH_NO_TA_SELECTED_CONTENT);
                }
            }
            int row = cell.getRow();
            cell.getTableView().refresh();    
        }
    }

    public void processTypeTA() {
        app.getFoolproofModule().updateControls(OH_FOOLPROOF_SETTINGS);
    }
    
    public void processTypeEditTA(String name, String email, String type, Button okBtn, 
        TextField nameTextField, TextField emailTextField) {
        
        AppGUIModule gui = app.getGUIModule();
        // FOOLPROOF DESIGN STUFF FOR ADD TA BUTTON
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        Button addTAButton = okBtn;
        boolean isLegal = data.isLegalNewTA(name, email, type);
        if (isLegal) {
            nameTextField.setOnAction(addTAButton.getOnAction());
            emailTextField.setOnAction(addTAButton.getOnAction());
            emailTextField.setStyle("-fx-text-inner-color: black;");
            nameTextField.setStyle("-fx-text-inner-color: black;");
        }
        else {
            nameTextField.setOnAction(null);
            emailTextField.setOnAction(null);
            if (!data.isLegalNewEmail(email)) {
                emailTextField.setStyle("-fx-text-inner-color: red;");
            } else {
                emailTextField.setStyle("-fx-text-inner-color: black;");
            }
            if (!data.isLegalNewName(name, email, type)) {
                 nameTextField.setStyle("-fx-text-inner-color: red;");
            } else {
                nameTextField.setStyle("-fx-text-inner-color: black;");
            }
           
        }
        addTAButton.setDisable(!isLegal);
    }
    
    public void editTA() {
        EditTADialog editTADialog = new EditTADialog(app);
        editTADialog.showAndWait(); 
    }
    
    public void editTAInformation(String name, String email, String type) {
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        
        TableView<TimeSlot> tasTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        ObservableList<TablePosition> selectedCells = tasTableView.getSelectionModel().getSelectedCells();
        TablePosition cell = selectedCells.get(0);
        int row = cell.getRow();
        
        ObservableList tas = tasTableView.getItems();
        TeachingAssistantPrototype currentTA = new TeachingAssistantPrototype("","","");
        TeachingAssistantPrototype temp = (TeachingAssistantPrototype)tas.get(row);
        
        currentTA.setName(name);
        currentTA.setEmail(email);
        currentTA.setType(type);
        
        EditTA_Transaction editTransaction = new EditTA_Transaction(data, temp, currentTA, row);
        app.processTransaction(editTransaction);
    }
    
    public void highlightTA() {
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        
        TableView<TimeSlot> tasTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        ObservableList<TablePosition> selectedCells = tasTableView.getSelectionModel().getSelectedCells();
        TablePosition cell = selectedCells.get(0);
        int row = cell.getRow();
        
        ObservableList tas = tasTableView.getItems();
        TeachingAssistantPrototype currentTA = (TeachingAssistantPrototype)tas.get(row);
        
        data.highlightCells(currentTA);
    }
}
