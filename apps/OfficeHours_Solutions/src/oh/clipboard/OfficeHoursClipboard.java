package oh.clipboard;

import djf.components.AppClipboardComponent;
import djf.modules.AppGUIModule;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_TAS_TABLE_VIEW;
import oh.data.OfficeHoursData;
import oh.data.TAOfficeHours;
import oh.data.TeachingAssistantPrototype;
import oh.data.TimeSlot;
import oh.transactions.CutTA_Transaction;
import oh.transactions.PasteTA_Transaction;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursClipboard implements AppClipboardComponent {
    OfficeHoursApp app;
    ArrayList<TeachingAssistantPrototype> clipboardItems;
    HashMap <TeachingAssistantPrototype, ArrayList<TAOfficeHours>> taOH;
    
    public OfficeHoursClipboard(OfficeHoursApp initApp) {
        app = initApp;
        clipboardItems = new ArrayList<>();
        taOH = new HashMap();
    }
    
    @Override
    public void cut() {
        AppGUIModule gui = app.getGUIModule();
        CutTA_Transaction cutTATransaction = new CutTA_Transaction(this);
        app.processTransaction(cutTATransaction);
    }

    @Override
    public void copy() {  
        AppGUIModule gui = app.getGUIModule();
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
        clipboardItems.add(ta);
        app.getFoolproofModule().updateAll();
    }
    
    public void cutTA() {
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
        ArrayList<TAOfficeHours> temp = data.getOfficeHours(ta);
        taOH.put(ta, temp);
        
        data.removeTA(ta);
        data.refreshTATable();
        data.refreshOHTable();
        clipboardItems.add(ta);
        app.getFoolproofModule().updateAll();
    }
    
    public void cutPastedTA(TeachingAssistantPrototype pastedTA) {
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        TeachingAssistantPrototype ta = pastedTA;
        
        data.removeTA(ta);
        data.refreshTATable();
        data.refreshOHTable();
        app.getFoolproofModule().updateAll();
    }
    
    public void pasteTA() {
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        TeachingAssistantPrototype ta = clipboardItems.get(clipboardItems.size() - 1);
        TeachingAssistantPrototype temp = new TeachingAssistantPrototype("","","");
        
        temp.setEmail(ta.getEmail());
        temp.setName(ta.getName());
        temp.setType(ta.getType());
        
        if (!data.isLegalNewEmail(temp.getEmail())) {
            String email = temp.getEmail();
            int atIndex = email.indexOf('@');
            email = email.substring(0, atIndex) + (int)(Math.random() * 100) + email.substring(atIndex);
            temp.setEmail(email);
        } 
        if (!data.isLegalNewName(temp.getName(), temp.getEmail(), temp.getType())) {
            temp.setName(temp.getName() + ((int)(Math.random() * 100)));
        }
        
        if (taOH.containsKey(ta)) {
            data.putTAOH(taOH.get(ta), ta);
        }
        
        data.addTA(temp);
        data.refreshTATable();
        data.refreshOHTable();
    }
    
    @Override
    public void paste() {
        AppGUIModule gui = app.getGUIModule();
        PasteTA_Transaction pasteTATransaction = new PasteTA_Transaction(this, clipboardItems.get(clipboardItems.size() - 1));
        app.processTransaction(pasteTATransaction);
    }    

    @Override
    public boolean hasSomethingToCut() {
        return ((OfficeHoursData)app.getDataComponent()).isTASelected();
    }

    @Override
    public boolean hasSomethingToCopy() {
        return ((OfficeHoursData)app.getDataComponent()).isTASelected();
    }

    @Override
    public boolean hasSomethingToPaste() {
        if ((clipboardItems != null) && (!clipboardItems.isEmpty()))
            return true;
        else
            return false;
    }
}