/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oh.transactions;

import jtps.jTPS_Transaction;
import oh.clipboard.OfficeHoursClipboard;


/**
 *
 * @author Michael
 */
public class CutTA_Transaction implements jTPS_Transaction{
     OfficeHoursClipboard clipboard;
    
    public CutTA_Transaction(OfficeHoursClipboard initClipboard) {
        clipboard = initClipboard;
    }

    @Override
    public void doTransaction() {
        clipboard.cutTA();
    }

    @Override
    public void undoTransaction() {
        clipboard.pasteTA();
    }
}
