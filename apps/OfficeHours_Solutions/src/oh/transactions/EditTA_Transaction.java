/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oh.transactions;

import jtps.jTPS_Transaction;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;

/**
 *
 * @author Michael
 */
public class EditTA_Transaction implements jTPS_Transaction {
    OfficeHoursData data;
    TeachingAssistantPrototype oldTA;
    TeachingAssistantPrototype editTA;
    int row;
    
    public EditTA_Transaction(OfficeHoursData initData, TeachingAssistantPrototype initTA, TeachingAssistantPrototype editTA, int row) {
        data = initData;
        oldTA = initTA;
        this.editTA = editTA;
        this.row = row;
    }

    @Override
    public void doTransaction() {
        data.editTA(oldTA, editTA, row);        
    }

    @Override
    public void undoTransaction() {
        data.editTA(editTA, oldTA, row);
    }
}
