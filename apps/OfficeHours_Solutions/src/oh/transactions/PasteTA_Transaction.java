/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oh.transactions;

import jtps.jTPS_Transaction;
import oh.clipboard.OfficeHoursClipboard;
import oh.data.TeachingAssistantPrototype;

/**
 *
 * @author Michael
 */
public class PasteTA_Transaction implements jTPS_Transaction{
     OfficeHoursClipboard clipboard;
     TeachingAssistantPrototype ta;
    
    public PasteTA_Transaction(OfficeHoursClipboard initClipboard, TeachingAssistantPrototype ta) {
        clipboard = initClipboard;
        this.ta = ta;
    }

    @Override
    public void doTransaction() {
        clipboard.pasteTA();
    }

    @Override
    public void undoTransaction() {
        clipboard.cutPastedTA(ta);
    }
}